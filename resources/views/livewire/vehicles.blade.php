<section>
    <form class="mb-4 flex space-x-4" action="#">
        <div class="flex-1 min-w-0">
            <label for="search" class="sr-only">Search</label>
            <div class="relative rounded-md shadow">
                <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                    <svg class="h-5 w-5 text-gray-400" x-description="Heroicon name: solid/search" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
                    </svg>
                </div>
                <input wire:model="search" type="search" name="search" id="search" class="p-3 d-flex focus:ring-pink-500 focus:border-pink-500 block w-full pl-10 sm:text-sm border-gray-300 rounded-md" placeholder="Search">
            </div>
        </div>
    </form>

    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="table table--dashboard w-full max-w-7xl">
                        <thead>
                        <tr>
                            <td class="cell cell--id">
                                <a wire:click.prevent="sortBy('id')" role="button" href="#">
                                    #
                                    @include('partials.sortable-icon', ['field' => 'id'])
                                </a>
                            </td>
                            <td>
                                <a wire:click.prevent="sortBy('model')" role="button" href="#">
                                    {{ __('Model') }}
                                    @include('partials.sortable-icon', ['field' => 'model'])
                                </a>
                            </td>
                            <td>
                                {{ __('Manufacturer') }}
                            </td>
                            <td>
                                <a wire:click.prevent="sortBy('price')" role="button" href="#">
                                    {{ __('Price') }}
                                    @include('partials.sortable-icon', ['field' => 'price'])
                                </a>
                            </td>
                            <td>
                                <a wire:click.prevent="sortBy('manufactured_at')" role="button" href="#">
                                    {{ __('Manufactured at') }}
                                    @include('partials.sortable-icon', ['field' => 'manufactured_at'])
                                </a>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        @if (!empty($vehicles))
                            @foreach($vehicles as $vehicle)
                                <tr onclick="Livewire.emit('openModal', 'show-vehicle', {{ json_encode([$vehicle]) }})">
                                    <td class="cell--thumbnail">
                                        {{ $vehicle->getFirstMedia('image') }}
                                    </td>
                                    <td>
                                        {{ $vehicle->model }}
                                    </td>
                                    <td>
                                        {{ $vehicle->manufacturer->name }}
                                    </td>
                                    <td>
                                        &euro; {{ number_format($vehicle->price, 2, ',', '.') }}
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($vehicle->manufactured_at)->format('d-m-Y h:i') }}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    {{ __('No results found.') }}
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>
