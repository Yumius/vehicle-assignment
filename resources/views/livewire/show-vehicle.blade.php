<div class="px-4 pt-5 pb-4">
    <div class="sm:flex">
        <div class="mb-4 flex-shrink-0 sm:mb-0 sm:mr-4">
            <div class="w-64">
                {{ $vehicle->getFirstMedia('image') }}
            </div>
        </div>
        <div>
            <h4 class="text-lg font-bold">
                {{ $vehicle->model }}
            </h4>
            <p class="mt-1">
                {{ $vehicle->description }}
            </p>
            <p class="mt-1">
                Price: &euro; {{ number_format($vehicle->price, 2, ',', '.') }}
            </p>
            <p class="mt-1">
                Manufactured at: {{ \Carbon\Carbon::parse($vehicle->manufactured_at)->format('d-m-Y h:i') }}
            </p>
        </div>
    </div>
</div>
