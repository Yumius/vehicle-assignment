<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gravity - Development assigment</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @livewireStyles
</head>
<body class="antialiased">
<div class="relative min-h-screen bg-gray-50 dark:bg-gray-900 py-4 sm:pt-5">
    <div class="max-w-7xl mx-auto">
        @include('partials.flash')

        @yield('content')
    </div>
</div>

@livewire('livewire-ui-modal')

<!-- Scripts -->
<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
<script src="{{ mix('js/app.js') }}" defer></script>

@livewireScripts

</body>
</html>
