<?php

namespace Database\Seeders;

use App\Models\Manufacturer;
use App\Models\Vehicle;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class ImportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $response = Http::get('https://gravity.nl/intake/api9.php')->throw(function ($response, $e) {
            if (!$response->successful()) {
                throw new Exception('Unable to handle request.');
            }
        })->collect();

        foreach ($response['vehicles'] as $key => $vehicle) {

            $dbManufacturer = Manufacturer::updateOrCreate([
                'name' => $vehicle['manufacturer']
            ], [
                'name' => $vehicle['manufacturer']
            ]);

            $dbVehicle = Vehicle::updateOrCreate([
                'manufacturer_id' => $dbManufacturer->id,
                'model'           => $vehicle['model'],
                'description'     => $vehicle['description'],
                'price'           => number_format(intval($vehicle['price']), 5, '.', ''),
                'currency'        => $vehicle['price_currency'],
                'manufactured_at' => Carbon::parse($vehicle['manufactured_at'])
            ], [
                'model' => $vehicle['model'],
            ]);

            if (isset($vehicle['image_url'])) {
                $dbVehicle->addMediaFromUrl($vehicle['image_url'])
                    ->withResponsiveImages()
                    ->toMediaCollection('image');
            }
        }
    }
}
