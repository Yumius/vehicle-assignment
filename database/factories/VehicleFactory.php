<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Manufacturer;
use App\Models\Vehicle;

class VehicleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vehicle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'manufacturer_id' => Manufacturer::factory(),
            'model' => $this->faker->name,
            'description' => $this->faker->text,
            'currency' => 'EUR',
            'price' => $this->faker->randomFloat(5, 0, 99999.99999),
            'manufactured_at' => $this->faker->dateTime(),
        ];
    }
}
