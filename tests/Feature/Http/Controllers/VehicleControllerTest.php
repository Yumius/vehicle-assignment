<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\VehicleController
 */
class VehicleControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $vehicles = Vehicle::factory()->count(3)->create();

        $response = $this->get(route('vehicle.index'));

        $response->assertOk();
        $response->assertViewIs('vehicle.index');
        $response->assertViewHas('vehicles');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('vehicle.create'));

        $response->assertOk();
        $response->assertViewIs('vehicle.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\VehicleController::class,
            'store',
            \App\Http\Requests\VehicleStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $manufacturer_id = $this->faker->randomNumber();
        $model = $this->faker->word;
        $description = $this->faker->text;
        $currency = $this->faker->word;
        $price = $this->faker->randomFloat(/** decimal_attributes **/);
        $manufactured_at = $this->faker->dateTime();

        $response = $this->post(route('vehicle.store'), [
            'manufacturer_id' => $manufacturer_id,
            'model' => $model,
            'description' => $description,
            'currency' => $currency,
            'price' => $price,
            'manufactured_at' => $manufactured_at,
        ]);

        $vehicles = Vehicle::query()
            ->where('manufacturer_id', $manufacturer_id)
            ->where('model', $model)
            ->where('description', $description)
            ->where('currency', $currency)
            ->where('price', $price)
            ->where('manufactured_at', $manufactured_at)
            ->get();
        $this->assertCount(1, $vehicles);
        $vehicle = $vehicles->first();

        $response->assertRedirect(route('vehicle.index'));
        $response->assertSessionHas('vehicle.id', $vehicle->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $vehicle = Vehicle::factory()->create();

        $response = $this->get(route('vehicle.show', $vehicle));

        $response->assertOk();
        $response->assertViewIs('vehicle.show');
        $response->assertViewHas('vehicle');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $vehicle = Vehicle::factory()->create();

        $response = $this->get(route('vehicle.edit', $vehicle));

        $response->assertOk();
        $response->assertViewIs('vehicle.edit');
        $response->assertViewHas('vehicle');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\VehicleController::class,
            'update',
            \App\Http\Requests\VehicleUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $vehicle = Vehicle::factory()->create();
        $manufacturer_id = $this->faker->randomNumber();
        $model = $this->faker->word;
        $description = $this->faker->text;
        $currency = $this->faker->word;
        $price = $this->faker->randomFloat(/** decimal_attributes **/);
        $manufactured_at = $this->faker->dateTime();

        $response = $this->put(route('vehicle.update', $vehicle), [
            'manufacturer_id' => $manufacturer_id,
            'model' => $model,
            'description' => $description,
            'currency' => $currency,
            'price' => $price,
            'manufactured_at' => $manufactured_at,
        ]);

        $vehicle->refresh();

        $response->assertRedirect(route('vehicle.index'));
        $response->assertSessionHas('vehicle.id', $vehicle->id);

        $this->assertEquals($manufacturer_id, $vehicle->manufacturer_id);
        $this->assertEquals($model, $vehicle->model);
        $this->assertEquals($description, $vehicle->description);
        $this->assertEquals($currency, $vehicle->currency);
        $this->assertEquals($price, $vehicle->price);
        $this->assertEquals($manufactured_at, $vehicle->manufactured_at);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $vehicle = Vehicle::factory()->create();

        $response = $this->delete(route('vehicle.destroy', $vehicle));

        $response->assertRedirect(route('vehicle.index'));

        $this->assertDeleted($vehicle);
    }
}
