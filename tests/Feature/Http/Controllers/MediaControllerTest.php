<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Medium;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\MediaController
 */
class MediaControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $media = Media::factory()->count(3)->create();

        $response = $this->get(route('medium.index'));

        $response->assertOk();
        $response->assertViewIs('medium.index');
        $response->assertViewHas('media');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('medium.create'));

        $response->assertOk();
        $response->assertViewIs('medium.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\MediaController::class,
            'store',
            \App\Http\Requests\MediaStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $uuid = $this->faker->uuid;
        $name = $this->faker->name;
        $file_name = $this->faker->word;
        $mime_type = $this->faker->word;
        $size = $this->faker->numberBetween(-100000, 100000);

        $response = $this->post(route('medium.store'), [
            'uuid' => $uuid,
            'name' => $name,
            'file_name' => $file_name,
            'mime_type' => $mime_type,
            'size' => $size,
        ]);

        $media = Medium::query()
            ->where('uuid', $uuid)
            ->where('name', $name)
            ->where('file_name', $file_name)
            ->where('mime_type', $mime_type)
            ->where('size', $size)
            ->get();
        $this->assertCount(1, $media);
        $medium = $media->first();

        $response->assertRedirect(route('medium.index'));
        $response->assertSessionHas('medium.id', $medium->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $medium = Media::factory()->create();

        $response = $this->get(route('medium.show', $medium));

        $response->assertOk();
        $response->assertViewIs('medium.show');
        $response->assertViewHas('medium');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $medium = Media::factory()->create();

        $response = $this->get(route('medium.edit', $medium));

        $response->assertOk();
        $response->assertViewIs('medium.edit');
        $response->assertViewHas('medium');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\MediaController::class,
            'update',
            \App\Http\Requests\MediaUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $medium = Media::factory()->create();
        $uuid = $this->faker->uuid;
        $name = $this->faker->name;
        $file_name = $this->faker->word;
        $mime_type = $this->faker->word;
        $size = $this->faker->numberBetween(-100000, 100000);

        $response = $this->put(route('medium.update', $medium), [
            'uuid' => $uuid,
            'name' => $name,
            'file_name' => $file_name,
            'mime_type' => $mime_type,
            'size' => $size,
        ]);

        $medium->refresh();

        $response->assertRedirect(route('medium.index'));
        $response->assertSessionHas('medium.id', $medium->id);

        $this->assertEquals($uuid, $medium->uuid);
        $this->assertEquals($name, $medium->name);
        $this->assertEquals($file_name, $medium->file_name);
        $this->assertEquals($mime_type, $medium->mime_type);
        $this->assertEquals($size, $medium->size);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $medium = Media::factory()->create();
        $medium = Medium::factory()->create();

        $response = $this->delete(route('medium.destroy', $medium));

        $response->assertRedirect(route('medium.index'));

        $this->assertDeleted($medium);
    }
}
