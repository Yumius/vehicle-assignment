<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Vehicle extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'manufacturer_id',
        'model',
        'description',
        'currency',
        'price',
        'manufactured_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'manufacturer_id' => 'integer',
        'price' => 'decimal:5',
        'manufactured_at' => 'timestamp',
    ];


    public function manufacturer()
    {
        return $this->belongsTo(\App\Models\Manufacturer::class);
    }
}
