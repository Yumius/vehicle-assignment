<?php

namespace App\Http\Livewire;

use App\Models\Vehicle;
use LivewireUI\Modal\ModalComponent;

class ShowVehicle extends ModalComponent
{

    public Vehicle $vehicle;

    public function mount(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }

    public function render()
    {
        return view('livewire.show-vehicle');
    }
}
