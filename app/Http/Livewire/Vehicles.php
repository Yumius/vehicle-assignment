<?php

namespace App\Http\Livewire;

use App\Models\Vehicle;
use App\Traits\FilteringTrait;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;
use File;

class Vehicles extends Component
{
    use WithPagination;
    use FilteringTrait;

    public string $sortField    = 'manufactured_at';
    public bool   $sortAsc      = false;
    public string $search       = '';
    public string $manufacturer = '';

    /**
     * @return View
     */
    public function render(): View
    {
        $vehicles = $this->query()->paginate();

        return view('livewire.vehicles', compact('vehicles'));
    }

    /**
     * @return Builder
     */
    private function query(): Builder
    {
        $query = Vehicle::query()
            ->select([
                '*',
            ])
            ->where(function (Builder $query) {
                $query
                    ->where(
                        DB::raw("CONCAT(model,' ',description)"), 'like', '%' . $this->search . '%'
                    );
            });

        $query->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc');

        return $query;
    }
}
