<?php

namespace App\Http\Controllers;

use App\Models\Manufacturer;
use App\Models\Vehicle;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Http;

class ImportController extends Controller
{
    /**
     * Run the Gravity import.
     *
     * @return RedirectResponse|void
     */
    public function gravity()
    {
        $response = Http::get('https://gravity.nl/intake/api.php');

        if (!$response->successful()) {
            $message = __('Unable to import Gravity feed.');

            if ($response->status() == '404') {
                $message = __('Gravity feed not found.');
            } else {
                $message .= ' E: ' . $response->status();
            }

            return redirect()->route('dashboard.index')
                ->with('error', $message);
        }

        $response->collect();

        if ($response['vehicles']) {
            foreach ($response['vehicles'] as $key => $vehicle) {

                $dbManufacturer = Manufacturer::updateOrCreate([
                    'name' => $vehicle['manufacturer']
                ], [
                    'name' => $vehicle['manufacturer']
                ]);

                $dbVehicle = Vehicle::updateOrCreate([
                    'manufacturer_id' => $dbManufacturer->id,
                    'model'           => $vehicle['model'],
                    'description'     => $vehicle['description'],
                    'price'           => number_format(intval($vehicle['price']), 5, '.', ''),
                    'currency'        => $vehicle['price_currency'],
                    'manufactured_at' => Carbon::parse($vehicle['manufactured_at'])
                ], [
                    'model' => $vehicle['model'],
                ]);

                if (isset($vehicle['image_url'])) {
                    $dbVehicle->addMediaFromUrl($vehicle['image_url'])
                        ->withResponsiveImages()
                        ->toMediaCollection('image');
                }
            }

            return redirect()->route('dashboard.index')
                ->with('success', __('Gravity feed successful imported'));
        }

    }
}
