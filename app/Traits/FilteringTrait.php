<?php

namespace App\Traits;

trait FilteringTrait
{
    /**
     * @param string $field
     */
    public function sortBy(string $field): void
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }
}
